<header>
	<!--Main navigation pannel-->
	<nav class="nav-main">
		<ul>
			<li><a href="online-shop.php">Gift Cards</a></li>
			<li><a href="online-shop.php">X-Mas Gifts</a></li>
			<li><a href="online-shop.php">Best Deals</a></li>
			<li><a href="online-shop.php">Gallery</a></li>
			<li class="add-item"><a href="add-item.php">Add Item</a></li>
			<li class="delete-item">
				<!--Add dropdown btn for mass delete-->
				<div class="dropdown">
					<a id="delete-one" href="#">Delete</a>
					<div class="dropdown-content">
						<a id="delete-all" href="#">Delete all</a>
					</div>
				</div></li>
		</ul>
	</nav>
</header>