<form id="main-form" action="php/add-items.php" method="post" class="item-info centerize">
	<div class="centerize">
		<div class="info-center"><p>SKU</p><input id="add-sku" type="text" name="sku" placeholder="SKU"><br></div>
		<div class="info-center"><p>Name</p><input id="add-name" type="text" name="name" placeholder="Name"><br></div>
		<div class="info-center"><p>Price</p><input id="add-price" type="text" name="price" placeholder="Price"><br></div>
	</div>

	<!--Chooses Item Type-->
	<div class="centerize">
		<select id="item-type" class="item-type">
			<option value="size">Electronics</option>
			<option value="dimensions">Houshold</option>
			<option value="weight">Heavy Duty</option>
		</select>
	</div>
	<!--Description based on item type-->
	<div class="description">
		<div class="info-center size"><p>gb</p><input id="size" type="text" name="size" placeholder="Size"><br></div>
		<div class="info-center weight"><p>kg</p><input id="weight" type="text" name="weight" placeholder="Weight"><br></div>
		<div class="info-center dimensions"><p>cm</p><input id="width" type="text" name="width" placeholder="Width"><br></div>
		<div class="info-center dimensions"><p>cm</p><input id="length" type="text" name="length" placeholder="Length"><br></div>
		<div class="info-center dimensions"><p>cm</p><input id="height" type="text" name="height" placeholder="Height"><br></div>
		<div class="info size">Please provide information about the item's size.</div>
		<div class="info weight">Please provide information about the item's weight, so that we can calculate the cost of delivery.</div>
		<div class="info dimensions">Please provide information about the item's dimensions, so that we know what packedging method to use.</div>
		<button id="btn-add" class="btn-add" type="button" name="add-product">Add Product</button><br>
		<p id="form-msg"></p>
	</div>
</form>