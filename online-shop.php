<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/online-shop.css">
	<title>Greatest Online Shop</title>
	<script
	src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>
	
</head>
<body>
	<!--Header-->
	<?php include dirname(__FILE__).'/includes/header.inc.php'; ?>
	<!--Div where items will be added-->
	<div id="item-area" class="item-area"></div>
	<footer></footer>
	<script src="js/display-items.js"></script>
	<script src="js/item.js"></script>	
	<script src="js/delete-item.js"></script>			
</body>
</html>