
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/online-shop.css">
	<title>Greatest Online Shop</title>
	<script
	src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>
</head>
<body>
	<?php include dirname(__FILE__).'/includes/header.inc.php'; ?>
	<!--Main area for selecting product parameters-->
	<div id="add-nav" class="add-nav">
		<h3 class="title">Product Details</h3>
		<!--Form that collects item attributes-->
		<?php include dirname(__FILE__).'/includes/form.inc.php'; ?>
	</div>
	<script src="js/add-item.js"></script>
</body>
</html>