<?php

abstract class AddAttribute extends BaseConnection
{
    //adds attribute to given SKU with foreign key
    public function addAttributes($attributeName, $attributeValue, $primeKey)
    {
        $sqlInsert = "INSERT INTO attributes (SKU, $attributeName) VALUES (?,?)";
        $stmt = $this->connectToDb()->prepare($sqlInsert);
        $stmt->execute(array($primeKey, $attributeValue));
    }

    // filters out special attributes
    public function manageAttributes($fieldValues)
    {
        $attributeName = '';
        $countValues = 0;
        $attributeValues = '';
        // depending on special attribute type, assign key and value
        foreach ($fieldValues as $key => $field) {
            if ($key != 'sku' && $key !='name' && $key != 'price' && $key !='btnAdd') {
                $countValues+=1;
                $attributeName .= ' '.$key ;
                $attributeValues .= ' '.$field;
            }
        }
        ($countValues>1) ?
        $this->addAttributes('dimensions', $attributeValues, $fieldValues['sku']) :
        $this->addAttributes($attributeName, $attributeValues, $fieldValues['sku']);
    }
}
