<?php

require_once __DIR__ . '/main-connection.php';
require_once __DIR__ . '/add-attribute.php';
require_once __DIR__ . '/get-all-items-query.php';
require_once __DIR__ . '/delete-items-query.php';
require_once __DIR__ . '/add-items-query.php';

//Depending on button pushed initiates class.
if (isset($_POST['btnDelete'])) {
    $delete = new DeleteItems($_POST['sku']);
} elseif (isset($_POST['fieldValues']['btnAdd'])) {
    $addItem = new AddItem($_POST['fieldValues']);
} else {
    $LoginUser = new ItemCollector();
}
