<?php

class AddItem extends AddAttribute
{
    public function __construct($fieldValues)
    {
        parent::__construct($fieldValues['sku']);
        $this->ItemName = $fieldValues['name'];
        $this->price = $fieldValues['price'];
        $this->checkSKU($fieldValues);
    }

    //Checks if SKU code is unique
    public function checkSKU($fieldValues)
    {
        $sql = "SELECT * FROM items WHERE SKU=?";
        $stmt = $this->connectToDb()->prepare($sql);
        $stmt->execute(array($this->sku));
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (isset($results[0])) {
            $this->errorList['errorSKU'] = true;
            echo json_encode($this->errorList);
            exit();
        }
        $this->addItem($fieldValues);
    }

    public function addItem($fieldValues)
    {
        $sql = "INSERT INTO items (SKU, Name, Price) VALUES (?,?,?)";
        $stmt = $this->connectToDb()->prepare($sql);
        $stmt->execute(array($this->sku, $this->ItemName, $this->price));
        $this->manageAttributes($fieldValues);
        echo json_encode($this->errorList);
    }
}
