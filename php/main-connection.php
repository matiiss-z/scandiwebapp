<?php

abstract class BaseConnection
{
    protected $sku;
    protected $errorList = ['errorSKU'=>false, 'errorEmty'=>false, 'errorFields' =>false];

    public function __construct($sku)
    {
        $this->sku = $sku;
    }

    protected function connectToDb()
    {
        $host = "localhost";
        $username = "root";
        $password = "";
        $database = "logincurcal";
        $charSet = "utf-8";
        try {
            $db = new PDO("mysql:host=$host; dbname=$database; charset = $charSet", $username, $password);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $db;
        } catch (PDOExeption $e) {
            echo "Connection failed: ".$e->getMessage();
        }
    }
}
