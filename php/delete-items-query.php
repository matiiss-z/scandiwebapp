<?php

class DeleteItems extends BaseConnection
{
    public function __construct($sku)
    {
        parent::__construct($sku);
        $this->deleteAll();
    }

    public function deleteAll()
    {
        $dbConnect = $this->connectToDb();
        if (empty($this->sku)) {
            $sql =("DELETE FROM items");
            $dbConnect->query($sql);
        //Deletes item by SKU code.
        } else {
            $sql =("DELETE FROM items WHERE SKU = ?");
            $stmt = $dbConnect->prepare($sql);
            $stmt->execute(array($this->sku));
        }
    }
}
