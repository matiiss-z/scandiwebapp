<?php

class ItemCollector extends BaseConnection
{
    public function __construct()
    {
        $this->getItems();
    }

    public function getItems()
    {
        $dbConnect = $this->connectToDb();
        $sql =("SELECT * FROM items");
        $sqlJoin = ("SELECT items.SKU, items.Name, items.Price, attributes.*
        FROM items
        INNER JOIN attributes ON items.SKU=attributes.SKU");
        $stmt = $dbConnect->query($sqlJoin);
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $itemCollection = array();
       
        foreach ($items as $value) {
            array_push($itemCollection, $value);
        }
        $json = json_encode($itemCollection);
        echo "$json";
    }
}
