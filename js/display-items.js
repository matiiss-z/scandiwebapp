
$(document).ready(() => {
	//Collects all items from database and creates list of items using ajax.done.	
	$.ajax("php/main-object.php").done(data => {
		const result = JSON.parse(data);
		for (let item in result){
			const newItem = new itemConst(result[item].SKU, result[item].Name, result[item].Price);

			newItem.Size = result[item].size;
			newItem.Weight = result[item].weight;
			newItem.Dimensions = result[item].Dimensions;
			drawItems(newItem);
		}
	}).fail(() => alert("Try again!"));	
});
