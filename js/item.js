//Main item Object
function itemConst(sku, itemName, price) {
  this.Sku = sku;
  this.itemName = itemName;
  this.Price = price;
  this.Size = 0;
  this.Weight = 0;
  this.Dimensions = 0;
}

//Draws Divs for each item in DB, taking into account special attribute. 
function drawItems(item) {
	const mainDiv = $('<div>',{class: 'item'});
	const chBox = $('<input>',{class:"delete", type:"checkbox", name:item.Sku})
	const skuP = $('<p>').text(`SKU:  ${item.Sku}`);
	const nameP = $('<p>').text(item.itemName);
	const priceP = $('<p>').text(`Price:  ${item.Price}$`);

	mainDiv.append(chBox, skuP, nameP, priceP);
	mainDiv.append(drawSize(item.Size));
	mainDiv.append(drawWeight(item.Weight));
	mainDiv.append(drawDimensions(item.Dimensions));
	mainDiv.appendTo("#item-area");
}

//3 functions that adds <p> depending on special attribute.
function drawSize(size) {
	return (!!size) ? $('<p>').text(`Size:  ${size}gb`) : false;
}

function drawWeight(weight) {
	return (!!weight) ? $('<p>').text(`Weight:  ${weight}kg`) : false;
}

function drawDimensions(dimensions) {
	if (!!dimensions) {
		let replaced = dimensions.split(' ').join('x');
		return $('<p>').text(`dimensions:  ${replaced}`);
	} else {
		return false;
	}
}
