//Deletes selected items(if checkbox: checked)
$(document).ready(() => {
$(".delete-item").on("click", function() {
	$(".delete").each(function() {
		if ($(this).is(":checked")) {
			const sku = $(this).attr('name');
			const btnDelete = $(".delete-item").val();
			$.ajax({
			type: 'POST',
			url: "php/main-object.php",
			data:{
				sku: sku,
				btnDelete: btnDelete
			}
			}).done(() => setTimeout(reloadPage,100))
		}
	});
});	
});

//Deletes all items in database.
$("#delete-all").on("click",() => {
	const sku = '';
	const btnDelete = $(".delete-item").val();
	$.ajax({
		type: 'POST',
		url: "php/main-object.php",
		data:{
			sku: sku,
			btnDelete:btnDelete
		}
	}).done(() => setTimeout(reloadPage,100));
});
function reloadPage(){
	location.reload();
}
