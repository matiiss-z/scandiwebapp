//Adds item to database using $.post
$("#btn-add").on("click", event => {
    $(document).ready(() => {
        event.preventDefault();
        const fields = document.getElementsByTagName("input");
        let fieldValues={"btnAdd":true};
        let allFieldsSubmited = 0;

        //Gets all values from input fields in form
        for (let field of fields) {
            if (field.value) {
                let valPair = {[field.name]: field.value};
                fieldValues = {...fieldValues, ...valPair};
            } else {
                allFieldsSubmited++;
            }
        }
        //Checks if all fields are submited
        if (allFieldsSubmited<5) {
            $.ajax({
                type: 'POST',
                url: "php/main-object.php",
                data:{
                    fieldValues: fieldValues 
                }
            //Checks if SKU is unique.
            }).done(data => {
                const result = JSON.parse(data);
                fieldChecks(result);
            });
        } else {
            fieldChecks({"errorEmty":true});
        }
    });

    function fieldChecks(result) {
        $("#add-sku, #add-name, #add-price, #size, #weight, #width, #length, #height ").removeClass("form-error");
        if (result.errorEmty == true) {
            $("#add-sku, #add-name, #add-price, #size, #weight, #width, #length, #height ").addClass("form-error");
            $("#form-msg").text("please fill in all fields!").css("color","red");
        } else if ( result.errorSKU == true) {
            $("#add-sku").addClass("form-error");
            $("#form-msg").text("the SKU code you selected is already taken!").css("color","red");
        }
        if (result.errorEmty == false && result.errorSKU == false ) {
            $("#form-msg").text("item has been successfully added to list!").css("color","green");
            $("#add-sku, #add-name, #add-price, #size, #weight, #width, #length, #height ").val("");
        }
    }
});

//preSelects special attribute.
$(document).ready(() => $("#item-type").val("weight").change());

//Displays a special attributes that corresponds to item type.
$("#item-type").on("change",() => {
    //resets values after change in attribute
    $("#size, #weight, #width, #length, #height").val('');
    const selectedItem = $("#item-type").val();
    $(".size, .dimensions, .weight").hide();
    $(`.${selectedItem}`).show();
});
